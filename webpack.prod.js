const merge = require('webpack-merge')
const webpack = require('webpack')
const common = require('./webpack.common.js')
module.exports = merge(common, {
  plugins: [
    new webpack.DefinePlugin({
      process: {
        env: {
          // Stringify necessary for text comparison
          // https://webpack.js.org/plugins/define-plugin/
          NODE_ENV: JSON.stringify('production')
        }
      }
    }),
    new webpack.optimize.UglifyJsPlugin()
  ]
});
