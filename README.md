# The magic of Reactive X: debouncing inputs to async functions

When a function is “expensive” we commonly want to try to reduce how many times we perform that operation.
When the input varies quickly a common way to reduce the number of function calls is to “debounce” the input.
However, when the function is async this creates some difficulties in providing an efficient solution that also provides a good user experience.
This code sample demonstrates our solution to this problem using the fantastic stream library Reactive X.

See the blog post [here](https://medium.com/p/f3d4d80ea68a).

## Running this code

```
git clone https://bitbucket.org/s_parkinson/rxjs-debounced-async
yarn install
yarn start
```
