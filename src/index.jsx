// MIT License

// Copyright (c) [year] [fullname]

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

import * as React from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import Rx from 'rxjs';

const request = (endpoint, page, search) =>
  axios.get(`https://swapi.co/api/${enpoint}`, { params: { page: page, search: search } })
    .then(response => ({ pages: reponse.count / 10, results: response.results }))

class StarWarsListView extends React.Component {

  state = {
    results: [],
    page: 1,
    pages: undefined,
  }

  // feed input changes into stream simply using BehaviourSubjects
  collection$ = new Rx.BehaviorSubject('people')
  search$ = new Rx.BehaviorSubject(null)
  page$ = new Rx.BehaviorSubject(1)
  requests$

  componentDidMount() {
    // combine control input streams
    const inputStream = this.requests$ = Rx.Observable.combineLatest([
      this.collection$, this.search$, this.page$
    ])

    const debounceTime = 500
    // merge debounce and throttle to get desired instant, debounced behaviour
    const debouncedThrottledInputStream = Rx.Observable
      .merge(
        inputStream.debounceTime(debounceTime),
        inputStream.throttleTime(debounceTime),
      )
      .distinctUntilChanged()

    // map changing inputs to axios request after delaying by 1ms
    const asyncObservablesStream = debouncedThrottledInputStream
      .delay(1)
      .map(inputs =>
        Rx.Observable.create(observer => {
          const CancelToken = axios.CancelToken;
          const source = CancelToken.source();

          axios.get(
            `https://swapi.co/api/${inputs[0]}`,
            {
              params: { page: inputs[2], search: inputs[1] },
              cancelToken: source.token,
            }
          )
          .then(response => observer.next(response))
          .catch(error => observer.error(error))

          return () => {
            // cancel request when disposed
            source.cancel('Operation canceled by a new input.')
          }
        })
      )

    // map input changes to just(null)
    const cancellingStream = inputStream
      .map(inputs => Rx.Observable.of(null))

    // merge, switch and subscribe
    this.requests$ = Rx.Observable
      .merge(
        asyncObservablesStream,
        cancellingStream
      )
      .switch()
      .subscribe(this.updateFromResponse, console.error)
  }

  componentWillUnmount() {
    // make sure we cancel observables when the component is disposed
    this.requests$.unsubscribe()
    this.collection$.unsubscribe()
    this.search$.unsubscribe()
    this.page$.unsubscribe()
  }

  updateFromResponse = response => {
    if (response === null) {
      this.setState({ results: null })
      return
    }

    const data = response.data
    this.setState({
      results: data.results,
      pages: parseInt(data.count / 10) + 1,
    })
  }

  render() {
    const {
      results,
      page,
      pages,
    } = this.state

    const rows = results ? results.map((result, i) => (
      <li key={i}>{result.name}</li>
    )) : <p>loading...</p>;

    return (
      <div>
        <div>
          <select
            onChange={this.handleCollectionChange}
            style={{ marginRight: 10 }}
          >
            <option value="people">People</option>
            <option value="starships">Starships</option>
            <option value="planets">Planets</option>
          </select>
          <input
            type='text'
            placeholder='search'
            onChange={this.handleSearchChange}
            style={{ marginRight: 10 }}
          />
          <button
            type="button"
            disabled={pages === undefined || page === 1}
            onClick={this.handlePreviousClick}
            style={{ marginRight: 10 }}
          >
            Previous page
          </button>
          <button
            type="button"
            disabled={pages === undefined || page >= pages}
            onClick={this.handleNextClick}>
            Next page
          </button>
        </div>
        <p>{pages ? `Showing page ${page} of ${pages}` : null}</p>
        <ul>
          {rows}
        </ul>
      </div>
    );
  }

  handleCollectionChange = (event) => {
    this.collection$.next(event.target.value)
    this.page$.next(1)
    this.setState({ page: 1, pages: undefined })
  }

  handleSearchChange = (event) => {
    this.search$.next(event.target.value && event.target.value.length > 0 ? event.target.value : null)
    this.page$.next(1)
    this.setState({ page: 1, pages: undefined })
  }

  handlePreviousClick = (event) => {
    this.page$.next(this.state.page)
    this.setState({ page: this.state.page - 1 })
  }

  handleNextClick = (event) => {
    this.page$.next(this.state.page + 1)
    this.setState({ page: this.state.page + 1 })
  }

}

ReactDOM.render(
  <StarWarsListView/>,
  document.getElementById('app'),
);
