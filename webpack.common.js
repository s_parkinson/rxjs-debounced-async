/*global module require __dirname */
const HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
  entry: {
    main: "./src/index.jsx"
  },

  output: {
    filename: "bundle.js",
    path: __dirname + "/dist",
    publicPath: "/"
  },

  resolve: {
    extensions: [".js", ".jsx", ".json"]
  },

  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: [
          'source-map-loader',
          {
            loader: 'babel-loader',
            options: {
              "presets": ["react", "es2015", "stage-0"]
            }
          },
        ]
      }
    ]
  },

  plugins: [
    new HtmlWebpackPlugin({
      title: "We love Star Wars",
      template: "src/index.html"
    })
  ]
};
