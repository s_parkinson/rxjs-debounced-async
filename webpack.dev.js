const merge = require('webpack-merge')
const webpack = require('webpack')
const common = require('./webpack.common.js')

module.exports = merge(common, {
  devServer: {
    port: 8083,
    host: '0.0.0.0',
    contentBase: "./dist",
    historyApiFallback: {
      index: "index.html",
      historyApiFallback: true
    }
  },
  plugins: [
    new webpack.DefinePlugin({
      process: {
        env: {
          // Stringify necessary for text comparison
          // https://webpack.js.org/plugins/define-plugin/
          NODE_ENV: JSON.stringify('development')
        }
      }
    })
  ],
  devtool: 'inline-source-map'
});
